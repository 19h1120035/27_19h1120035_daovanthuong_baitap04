﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Server
{
    public partial class frmServer : Form
    {
        public frmServer()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            
        }

        IPEndPoint ipep;
        Socket server;
        List<Socket> clientList;
        string name = "";

        /// <summary>
        /// Kết nối tới server
        /// </summary>
        void Connect()
        {
            clientList = new List<Socket>();

            ipep = new IPEndPoint(IPAddress.Any, int.Parse(txtPort.Text));
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            server.Bind(ipep);

            Thread listen = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        server.Listen(100);
                        Socket client = server.Accept();
                        clientList.Add(client);
                        Thread receive = new Thread(Receive);
                        receive.IsBackground = true;
                        receive.Start(client);
                    }
                }
                catch (Exception)
                {
                    ipep = new IPEndPoint(IPAddress.Any, int.Parse(txtPort.Text)); //9999: port
                    server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                }
            });
            listen.IsBackground = true;
            listen.Start();

        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        void Close()
        {
            server.Close();
        }

        /// <summary>
        /// Gửi tin
        /// </summary>
        void Send(Socket client)
        {
            if (client != null && txtMessage.Text != string.Empty)
            {
                client.Send(Serialize($"$ {txtTen.Text} >> "+txtMessage.Text));
            }
        }

        /// <summary>
        /// Nhận tin
        /// </summary>
        void Receive(object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);

                    foreach (Socket item in clientList)
                    {
                        if (item != null && item != client)
                        {
                            item.Send(Serialize(message));

                        }

                    }

                    AddMessage(message);
                }
            }
            catch (Exception ex)
            {
                clientList.Remove(client);
                client.Close();
            }


        }

        /// <summary>
        /// Add message vào khung chat
        /// </summary>
        /// <param name="str"></param>
        void AddMessage(string str)
        {
            txtResult.Text += str +"\r\n";
            txtMessage.Clear();
            txtMessage.Focus();
        }

        /// <summary>
        /// Phân mảnh
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh lại
        /// </summary>
        /// <returns></returns>
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }

        /// <summary>
        /// Gửi tin cho tất cả client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (txtTen.Text == "" || btnStart.Enabled == true)
            {
                MessageBox.Show("Vui lòng nhập tên vào start");
            }
            else
            {
                if (txtMessage.Text != string.Empty)
                {
                    foreach (Socket item in clientList)
                    {
                        Send(item);
                    }
                    AddMessage($"# {txtTen.Text} >> " + txtMessage.Text);
                }
            }
        }

        /// <summary>
        /// Bắt đầu nhận kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            Connect();
            btnStart.Enabled = false;
            txtMessage.Focus();
        }

        /// <summary>
        /// Đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            server.Close();
        }

        private void btnTen_Click(object sender, EventArgs e)
        {
            if (txtTen.Text != string.Empty)
            {
                btnTen.Enabled = false;
                txtTen.Enabled = false;
            }
            else
            {
                MessageBox.Show("Vui lòng nhập tên !");
            }
        }

        /// <summary>
        /// Đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmServer_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }
    }
}
