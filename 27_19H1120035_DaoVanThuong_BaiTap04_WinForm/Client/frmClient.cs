﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using Server;

namespace Client
{
    public partial class frmClient : Form
    {
        public frmClient()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        IPEndPoint ipep;
        Socket client;

        /// <summary>
        /// Kết nối tới server
        /// </summary>
        void Connect()
        {
            // IP: địa chỉ của server
            ipep = new IPEndPoint(IPAddress.Parse(txtIP.Text), int.Parse(txtPort.Text));
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                client.Connect(ipep);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể kết nối server !" + ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Thread listen = new Thread(Receive);

            // đặt một giá trị cho biết một chuỗi có phải là một chuỗi nền hay không
            // trả về true nếu chuỗi này đang hoặc sắp trở thành một chuỗi nền
            listen.IsBackground = true;
            listen.Start();
        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        void Close()
        {
            client.Close();
        }

        /// <summary>
        /// Gửi tin
        /// </summary>
        void Send()
        {
            if (txtMessage.Text != string.Empty)
            {
                client.Send(Serialize($"$ {txtTen.Text} >> " + txtMessage.Text));
            }
        }

        /// <summary>
        /// Nhận tin
        /// </summary>
        void Receive()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);
                    AddMessage(message);
                }
            }
            catch (Exception ex)
            {
                Close();
            }


        }

        /// <summary>
        /// Add message vào khung chat
        /// </summary>
        /// <param name="str"></param>
        void AddMessage(string str)
        {
            txtResult.Text += str + "\r\n";
            txtMessage.Clear();
            txtMessage.Focus();
        }

        /// <summary>
        /// Phân mảnh
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh lại
        /// </summary>
        /// <returns></returns>
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }


        /// <summary>
        /// Gửi tin đi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {
           
            if (txtTen.Text == "" || btnConnect.Enabled == true)
            {
                MessageBox.Show("Vui lòng nhập tên và Connect !");
                txtMessage.Clear();
            }
            else
            {
                if (txtMessage.Text != string.Empty)
                {
                    Send();
                    AddMessage($"# {txtTen.Text} >> " + txtMessage.Text);
                }
            }
        }

        /// <summary>
        /// Đóng kết nối
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Kết nối tới server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
            btnConnect.Enabled = false;
            txtMessage.Focus();
        }

        private void btnTen_Click(object sender, EventArgs e)
        {
            if (txtTen.Text != string.Empty)
            {
                btnTen.Enabled = false;
                txtTen.Enabled = false;
            }
            else
            {
                MessageBox.Show("Vui lòng nhập tên !");
            }
        }
    }
}
