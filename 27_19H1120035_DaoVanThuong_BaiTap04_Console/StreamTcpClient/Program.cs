﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace StreamTcpClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "StreamTcpClient";
            string strData;
            string input;

            // Thiết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);

            // Khởi tạo object của lớp Socket để sử dụng dịch vụ Tcp
            // Lưu ý Socket Type của Tcp là Stream
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                server.Connect(ipep); // Tạo kết nối với server
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                Console.WriteLine(e.Message);
                return;
            }

            // Khởi tạo NetworkStream với Socket được chỉ định dùng để gửi và nhận dữ liệu
            NetworkStream ns = new NetworkStream(server);

            // Khởi tạo StreamReader cho luồng được chỉ định để đọc
            StreamReader sr = new StreamReader(ns);

            // Khởi tạo StreamWriter cho luồng được chỉ định để viết
            StreamWriter sw = new StreamWriter(ns);



            while (true)
            {
                // Đọc một dòng ký tự từ luồng hiện tại và trả về dữ liệu dưới dạng một chuỗi
                strData = sr.ReadLine();
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write("$ Server >>> ");
                Console.ResetColor();
                Console.Write(strData + "\r\n");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("# Command >> ");
                Console.ResetColor();
                input = Console.ReadLine();
                sw.WriteLine(input); // Ghi chuỗi input vào luồng sw 

                // Xóa tất cả các bộ đệm cho trình ghi hiện tại
                // và khiến mọi dữ liệu trong bộ đệm được ghi vào luồng bên dưới.
                sw.Flush();
                if (input.ToUpper().Equals("EXIT")) break;
            }
            Console.WriteLine("Disconnecting from server...");
            sr.Close();  // Đóng StreamReader hiện tại và luồng bên dưới
            sw.Close();
            ns.Close();

            // Vô hiệu hóa gửi nhận trên socket
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }
    }
}
