﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VarTcpSrvr
{
    class Program
    {
        private static int SendVarData(Socket s, byte[] data)
        {
            int total = 0;
            int size = data.Length;
            int dataleft = size;
            int sent;

            byte[] datasize = new byte[4];// tạo mảng byte để gửi dữ liệu tới server

            // Trả về giá trị số nguyên có dấu 32 bit đã chỉ định dưới dạng một mảng byte.
            // value: Số lượng cần chuyển đổi.
            // Một mảng byte có độ dài là 4
            datasize = BitConverter.GetBytes(size);
            // gửi dữ liệu đến socket được kết nối
            // Tham số truyền vào là 1 mảng byte chứa dữ liệu được gửi
            sent = s.Send(datasize);

            while (total < size)
            {
                // gửi số byte dữ liệu được chỉ định tới socket được kết nối
                // truyền vào 1 mảng byte 
                // Vị trí trong bộ đệm dữ liệu để bắt đầu gửi dữ liệu.
                // Số byte cần gửi.
                // trả về Số byte được gửi đến Socket.
                sent = s.Send(data, total, dataleft, SocketFlags.None);
                total += sent;
                dataleft -= sent;
            }
            return total;
        }

        private static byte[] ReceiveVarData(Socket s)
        {
            int total = 0;
            int recv;
            byte[] datasize = new byte[4];

            // Nhận số byte được chỉ định từ Socket được ràng buộc vào vị trí bù đã chỉ định của bộ đệm nhận, sử dụng SocketFlags được chỉ định.
            // Truyền vào Một mảng Byte là vị trí lưu trữ dữ liệu đã nhận.
            //  Vị trí trong bộ đệm để lưu trữ dữ liệu đã nhận.
            // Số byte cần nhận.
            // Trả về số lượng byte nhận được.
            recv = s.Receive(datasize, 0, 4, 0);

            // Trả về một số nguyên có dấu 32 bit được chuyển đổi từ bốn byte tại một vị trí được chỉ định trong một mảng byte.
            // truyền vào mảng byte và vị trí bắt đầu trong giá trị
            int size = BitConverter.ToInt32(datasize, 0);
            int dataleft = size;
            byte[] data = new byte[size];// tạo mảng byte để nhận dữ liệu từ server
            while (total < size)
            {
                // Nhận số byte được chỉ định từ Socket được ràng buộc vào vị trí bù đã chỉ định của bộ đệm nhận, sử dụng SocketFlags được chỉ định.
                recv = s.Receive(data, total, dataleft, 0);
                if (recv == 0) // nếu số byte trả về bằng 0 thì biến đổi chuỗi exit thành mảng byte và dừng vòng lặp
                {
                    data = Encoding.ASCII.GetBytes("exit");
                    break;
                }
                total += recv;
                dataleft -= recv;
            }
            return data;
        }

        public static void Main()
        {
            Console.Title= "VarTcpServer";
            byte[] data = new byte[1024];

            // 1. Thết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            // Giá trị Any của IPAddress tương ứng với  IP của tất cả giao diện mạng trên máy
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);


            // Tcp sử dụng đồng thời hai Socket
            // 1 Socket để chờ nghe kết nối, một Socket để gửi/nhận dữ liệu
            // Socket server này chỉ làm nhiệm vụ chờ kết nối từ Client
            // Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket newsock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // 2. Kết nối Socket với IPEndPoint
            // Yều cầu hệ điều hành cho phép chiếm dụng cổng Tcp 6969
            // Server sẽ nghe trên tất cả các mạng mà máy tính này kết nối tới 
            // chỉ cần gói tin Tcp đến cổng 6969, tiến trình server sẽ nhận được
            newsock.Bind(ipep);

            // 3. Lắng nghe từ client
            newsock.Listen(10); // Lắng nghe tối đa từ 10 client
            Console.WriteLine("Waiting for a client...");

            // 4. Chấp nhận kết nối từ client
            // Tcp đòi hỏi 1 Socket thứ hai làm nhiệm vụ gửi/nhận dữ liệu
            Socket client = newsock.Accept();
            IPEndPoint newclient = (IPEndPoint)client.RemoteEndPoint;
            Console.WriteLine("Connected with {0} at port {1}", newclient.Address, newclient.Port);

            string welcome = "Welcome to my test server";

            // Biến đổi chuỗi strData thành mảng byte
            data = Encoding.ASCII.GetBytes(welcome);
            // gửi mảng byte đến client
            int sent = SendVarData(client, data);

            for (int i = 0; i < 7; i++)
            {
                data = ReceiveVarData(client); // nhận dữ liệu từ client và trả về 1 mảng byte chứa dữ liệu được nhận
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write("$ Client >> ");
                Console.ResetColor();
                Console.Write(Encoding.ASCII.GetString(data) + "\r\n"); // chuyển từ mảng byte sang chuỗi và in ra màn hình
            }
            Console.WriteLine("Disconnected from {0}", newclient.Address);
            client.Close(); // đóng socket
            newsock.Close();
        }
    }
}
