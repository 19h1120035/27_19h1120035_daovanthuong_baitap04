﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FixedTcpClient
{
    class Program
    {
        private static int SendData(Socket s, byte[] data)
        {
            int total = 0;
            int size = data.Length;
            int dataleft = size;
            int sent;

            while (total < size)
            {
                sent = s.Send(data, total, dataleft, SocketFlags.None);
                total += sent;
                dataleft -= sent;
            }
            return total;
        }

        private static byte[] ReceiveData(Socket s, int size)
        {
            int total = 0;
            int dataleft = size;
            byte[] data = new byte[size];
            int recv;

            while (total < size)
            {
                recv = s.Receive(data, total, dataleft, 0);
                if (recv == 0)
                {
                    data = Encoding.ASCII.GetBytes("exit ");
                    break;
                }
                total += recv;
                dataleft -= recv;
            }
            return data;
        }

        public static void Main()
        {
            Console.Title = "Fixed Tcp Client";
            byte[] data = new byte[1024];
            int sent;
            // Thiết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);

            // Khởi tạo object của lớp Socket để sử dụng dịch vụ Tcp
            // Lưu ý Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep);  // Tạo kết nối với server
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                Console.WriteLine(e.ToString());
                return;
            }

            int recv = server.Receive(data);

            // Chuyển đổi mảng byte data về chuỗi
            // Tham số truyền vào là 1 mảng byte cần chuyển đổi, chỉ số của byte đầu tiên cần chuyển, số byte nhận được từ receive
            string stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Write("# Receive >> ");
            Console.ResetColor();
            Console.Write(stringData + "\r\n");

            sent = SendData(server, Encoding.ASCII.GetBytes("message 1"));
            sent = SendData(server, Encoding.ASCII.GetBytes("message 2"));
            sent = SendData(server, Encoding.ASCII.GetBytes("message 3"));
            sent = SendData(server, Encoding.ASCII.GetBytes("message 4"));
            sent = SendData(server, Encoding.ASCII.GetBytes("message 5"));
            Console.WriteLine("Disconnecting from server...");
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }
    }
}
