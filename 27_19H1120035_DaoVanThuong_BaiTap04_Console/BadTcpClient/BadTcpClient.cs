﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace BadTcpClient
{
    class BadTcpClient
    {
        static void Main(string[] args)
        {
            Console.Title = "BadTcpClient";

            // Thiết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6969);

            // Khởi tạo object của lớp Socket để sử dụng dịch vụ Tcp
            // Lưu ý Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep); // Tạo kết nối với server
                while (true)
                {
                    // tạo mảng byte để lưu dữ liệu nhận được từ server
                    byte[] dataReceive = new byte[1024]; 

                    // Nhận mảng byte dataReceive từ server và lưu vào bộ bộ đệm
                    // trả về số byte nhận được
                    int receive = server.Receive(dataReceive);

                    // Chuyển đổi mảng byte dataReceive về chuỗi
                    // Tham số truyền vào là 1 mảng byte cần chuyển đổi, chỉ số của byte đầu tiên cần chuyển, số byte nhận được từ receive
                    string strData = Encoding.ASCII.GetString(dataReceive, 0, receive);

                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.Write("$ Server >>> ");
                    Console.ResetColor();
                    Console.Write(strData + "\n");

                    // nếu mà server gửi 'exit', thì client sẽ nhận 'goodbye!!!' và dừng 
                    if (strData.ToUpper().Equals("GOODBYE!!!")) break;

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("# Command >> ");
                    Console.ResetColor();
                    strData = Console.ReadLine();

                    // tạo mảng byte để lưu trữ dữ liệu gửi tới server
                    byte[] dataSend = new byte[1024];

                    // nếu client nhập vào: exit
                    if (strData.ToUpper().Equals("EXIT")) 
                    {
                        strData = "Goodbye!!!"; // thì server sẽ nhận lại chuỗi 'goodbye!!!'
                        dataSend = Encoding.ASCII.GetBytes(strData);

                        // thì gửi chuỗi goodbye dưới dạng byte đến cho server
                        server.Send(dataSend, dataSend.Length, SocketFlags.None);
                        break;
                    }
                    // Biến đổi chuỗi thành mảng byte
                    dataSend = Encoding.ASCII.GetBytes(strData);

                    // Gửi mảng byte data tới tiến trình server
                    server.Send(dataSend, dataSend.Length, SocketFlags.None);
                }
                Console.WriteLine("Disconnecting from server...");

                // Vô hiệu hóa gửi/ nhận trên socket
                server.Shutdown(SocketShutdown.Both); // Không tiếp tục nhận dữ liệu nữa
                server.Close(); // Đóng Socket và giải phóng tài nguyên
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                Console.WriteLine(e.Message);
                return;
            }
        }
    }
}
