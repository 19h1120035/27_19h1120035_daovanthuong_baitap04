﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkStreamTcpClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "NetworkStream Tcp Cient";
            byte[] data = new byte[1024];
            string input, strData;
            int receive;
            // Thiết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);

            // Khởi tạo object của lớp Socket để sử dụng dịch vụ Tcp
            // Lưu ý Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep); // Tạo kết nối với server
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                Console.WriteLine(e.ToString());
                return;
            }
            // Khởi tạo biến của NetworkStream từ tcp socket
            NetworkStream ns = new NetworkStream(server);


            while (true)
            {
                if (ns.CanRead) // nếu dữ liệu có thể đọc từ luồng
                {
                    // Đọc dữ liệu từ luồng ns
                    // Trả về số byte được đọc từ ns
                    receive = ns.Read(data, 0, data.Length);


                    // Chuyển đổi mảng byte data về chuỗi
                    // Tham số truyền vào là 1 mảng byte cần chuyển đổi, chỉ số của byte đầu tiên cần chuyển, số byte nhận được từ receive
                    strData = Encoding.ASCII.GetString(data, 0, receive);
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.Write("# Receive >> ");
                    Console.ResetColor();
                    Console.Write(strData + "\r\n");
                }
                else
                {
                    Console.WriteLine("Error: Can't read from this socket");
                    ns.Close();
                    server.Close();
                    return;
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("# Command >> ");
                Console.ResetColor();
                input = Console.ReadLine();
                if (input == "exit")
                    break;
                if (ns.CanWrite) // trả về true nếu dữ liệu có thể ghi được vào ns
                {
                    // Ghi dữ liệu vào ns
                    // Tham số truyền vào là 1 mảng data được chuyển từ chuỗi input thành mảng byte data, 
                    // Vị trí bộ đệm để bắt đầu ghi và số byte để ghi vào ns
                    ns.Write(Encoding.ASCII.GetBytes(input), 0, input.Length);
                    ns.Flush(); // Xóa dữ liệu khỏi luồng
                }
            }
            Console.WriteLine("Disconnecting from server...");
            ns.Close(); // đóng luồng hiện tại và giải phóng tài nguyên được liên kết với luồng hiện tại
            // vô hiệu hóa gửi và nhận
            server.Shutdown(SocketShutdown.Both); // không tiếp tục nhận dữ liệu nữa
            server.Close(); // đóng socket và giải phóng tài nguyên
        }
    }
}
