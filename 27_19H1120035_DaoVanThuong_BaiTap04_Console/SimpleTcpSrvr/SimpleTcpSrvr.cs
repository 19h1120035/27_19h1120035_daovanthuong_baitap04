﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace SimpleTcpSrvr
{
    class SimpleTcpSrvr
    {
        static void Main(string[] args)
        {
            Console.Title = "SimpleTcpServer";

            // Chương trình bên phía server gồm các công việc: 
            // 1. Thết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            // Giá trị Any của IPAddress tương ứng với  IP của tất cả giao diện mạng trên máy
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050); // khai báo địa chỉ IP và cổng port

            // Tcp sử dụng đồng thời hai Socket
            // 1 Socket để chờ nghe kết nối, một Socket để gửi/nhận dữ liệu
            // Socket server này chỉ làm nhiệm vụ chờ kết nối từ Client
            // Socket Type của Tcp là Stream
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // 2. Kết nối Socket với IPEndPoint
            // Yều cầu hệ điều hành cho phép chiếm dụng cổng Tcp 9050
            // Server sẽ nghe trên tất cả các mạng mà máy tính này kết nối tới 
            // chỉ cần gói tin Tcp đến cổng 9050, tiến trình server sẽ nhận được
            server.Bind(ipep);

            // 3. Lắng nghe từ client
            server.Listen(10); // lắng nghe tối ta từ 10 client
            Console.WriteLine("Waiting for a client...");

            // 4. Chấp nhận kết nối từ client
            // Tcp đòi hỏi 1 Socket thứ hai làm nhiệm vụ gửi/nhận dữ liệu
            Socket client = server.Accept();
            //Console.WriteLine($"Accept connect with: {client.RemoteEndPoint.ToString()}");
            IPEndPoint clientep = (IPEndPoint)client.RemoteEndPoint;
            Console.WriteLine("Connected with {0} at port {1}", clientep.Address, clientep.Port);

            // 5. Gửi nhận dữ liệu từ client.
            // Sau khi kết nối thì gửi cho client dòng chữ welcome.
            string strData = "Welcome to my test server ! ";
            // Tạo mảng byte để gửi dữ liệu
            byte[] dataSend = new byte[1024];

            // Biến đổi chuỗi strData thành mảng byte
            dataSend = Encoding.ASCII.GetBytes(strData);

            // Gửi mảng byte dataSend tới client
            client.Send(dataSend, dataSend.Length, SocketFlags.None);
            // Tạo vòng lặp để gửi nhận dữ liệu
            while (true)
            {
                // Tạo mảng byte để nhận dữ liệu client
                byte[] dataReceive = new byte[1024];

                // Nhận mảng byte dataReceive từ client và lưu vào bộ bộ đệm
                // trả về số byte nhận được
                int receive = client.Receive(dataReceive);

                // nếu số byte trả về = 0 thì dừng 
                if (receive == 0)
                    break;

                // Chuyển đổi mảng byte dataReceive về chuỗi
                // Tham số truyền vào là 1 mảng byte cần chuyển đổi, chỉ số của byte đầu tiên cần chuyển, số byte nhận được từ receive
                strData = Encoding.ASCII.GetString(dataReceive, 0, receive);

                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write("$ Client >>> ");
                Console.ResetColor();
                Console.Write(strData + "\n");

                // Gửi lại cho client dòng chữ in hoa
                strData = strData.ToUpper();

                // chuổi đổi chuỗi strData về mảng byte
                dataSend = Encoding.ASCII.GetBytes(strData);

                // gửi mảng byte đến cho client
                client.Send(dataSend, dataSend.Length, SocketFlags.None);
            }
            Console.WriteLine("Disconnected from {0}", clientep.Address);
            client.Close(); // Đóng Socket và giải phóng tài nguyên
            server.Close();
        }
    }
}