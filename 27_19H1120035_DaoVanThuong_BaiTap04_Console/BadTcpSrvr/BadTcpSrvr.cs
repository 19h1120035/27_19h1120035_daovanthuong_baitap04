﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace BadTcpSrvr
{
    class BadTcpSrvr
    {
        static void Main(string[] args)
        {
            Console.Title = "BadTcpServer";

            // 1. Thết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            // Giá trị Any của IPAddress tương ứng với  IP của tất cả giao diện mạng trên máy
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 6969);

            // Tcp sử dụng đồng thời hai Socket
            // 1 Socket để chờ nghe kết nối, một Socket để gửi/nhận dữ liệu
            // Socket server này chỉ làm nhiệm vụ chờ kết nối từ Client
            // Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // 2. Kết nối Socket với IPEndPoint
            // Yều cầu hệ điều hành cho phép chiếm dụng cổng Tcp 6969
            // Server sẽ nghe trên tất cả các mạng mà máy tính này kết nối tới 
            // chỉ cần gói tin Tcp đến cổng 6969, tiến trình server sẽ nhận được
            server.Bind(ipep);

            // 3. Lắng nghe từ client
            server.Listen(10); // Lắng nghe tối đa từ 10 client
            Console.WriteLine("Waiting for a client...");

            // 4. Chấp nhận kết nối từ client
            // Tcp đòi hỏi 1 Socket thứ hai làm nhiệm vụ gửi/nhận dữ liệu
            Socket client = server.Accept();
            string strData = "Welcome to my test server";

            // Tạo mảng byte để gửi dữ liệu tới client
            byte[] dataSend = new byte[1024];

            // Biến đổi chuỗi strData thành mảng byte
            dataSend = Encoding.ASCII.GetBytes(strData);

            // Gửi mảng byte dataSend tới client
            client.Send(dataSend, dataSend.Length, SocketFlags.None);
            IPEndPoint newclient = (IPEndPoint)client.RemoteEndPoint;
            Console.WriteLine("Connected with {0} at port {1}", newclient.Address, newclient.Port);

            // Tạo vòng lặp để gửi nhận dữ liệu
            while (true)
            {
                // Tạo mảng byte để nhận dữ liệu từ client
                byte[] dataReceive = new byte[1024];

                // Nhận mảng byte dataReceive từ client và lưu vào bộ bộ đệm
                // trả về số byte nhận được
                int receive = client.Receive(dataReceive);

                // Chuyển đổi mảng byte dataReceive về chuỗi
                // Tham số truyền vào là 1 mảng byte cần chuyển đổi, chỉ số của byte đầu tiên cần chuyển, số byte nhận được từ receive
                strData = Encoding.ASCII.GetString(dataReceive, 0, receive);

                //Console.WriteLine($"Client: {s}");
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.Write("$ Client >>> ");
                Console.ResetColor();
                Console.Write(strData + "\n");

                // nếu mà client gửi 'exit', thì server sẽ nhận 'goodbye!!!' và dừng 
                if (strData.ToUpper().Equals("GOODBYE!!!")) break;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("# Command >> ");
                Console.ResetColor();
                strData = Console.ReadLine();

                // nếu server nhập vào 'exit'
                if (strData.ToUpper().Equals("EXIT")) 
                {
                    // thì client sẽ nhận chuỗi goodbye!!!
                    strData = "Goodbye!!!";

                    // chuổi đổi chuỗi strData về mảng byte
                    dataSend = Encoding.ASCII.GetBytes(strData);

                    // gửi mảng byte đến cho client
                    client.Send(dataSend, dataSend.Length, SocketFlags.None); 
                    break; // dừng chương trình
                }
                // chuổi đổi chuỗi strData về mảng byte
                dataSend = Encoding.ASCII.GetBytes(strData);

                // gửi mảng byte đến cho client
                client.Send(dataSend, dataSend.Length, SocketFlags.None); 

            }
            Console.WriteLine("Disconnecting from {0}", newclient.Address);
            client.Close(); // Đóng Socket và giải phóng tài nguyên
            server.Close();
        }
    }
}
