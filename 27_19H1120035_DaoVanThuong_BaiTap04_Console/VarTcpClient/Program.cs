﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VarTcpClient
{
    class Program
    {
        private static int SendVarData(Socket s, byte[] data)
        {
            int total = 0;
            int size = data.Length;
            int dataleft = size;
            int sent;

            byte[] datasize = new byte[4]; // tạo mảng byte để gửi dữ liệu tới server

            // Trả về giá trị số nguyên có dấu 32 bit đã chỉ định dưới dạng một mảng byte.
            // value: Số lượng cần chuyển đổi.
            // Một mảng byte có độ dài là 4
            datasize = BitConverter.GetBytes(size);

            // gửi dữ liệu đến socket được kết nối
            // Tham số truyền vào là 1 mảng byte chứa dữ liệu được gửi
            sent = s.Send(datasize);

            while (total < size)
            {
                // gửi số byte dữ liệu được chỉ định tới socket được kết nối
                // truyền vào 1 mảng byte 
                // Vị trí trong bộ đệm dữ liệu để bắt đầu gửi dữ liệu.
                // Số byte cần gửi.
                // trả về Số byte được gửi đến Socket.
                sent = s.Send(data, total, dataleft, SocketFlags.None);
                total += sent;
                dataleft -= sent;
            }
            return total;
        }

        private static byte[] ReceiveVarData(Socket socket)
        {
            int total = 0;
            int receive;
            byte[] datasize = new byte[4];

            // Nhận số byte được chỉ định từ Socket được ràng buộc vào vị trí bù đã chỉ định của bộ đệm nhận, sử dụng SocketFlags được chỉ định.
            // Truyền vào Một mảng Byte là vị trí lưu trữ dữ liệu đã nhận.
            //  Vị trí trong bộ đệm để lưu trữ dữ liệu đã nhận.
            // Số byte cần nhận.
            // Trả về số lượng byte nhận được.
            receive = socket.Receive(datasize, 0, 4, 0);

            // Trả về một số nguyên có dấu 32 bit được chuyển đổi từ bốn byte tại một vị trí được chỉ định trong một mảng byte.
            // truyền vào mảng byte và vị trí bắt đầu trong giá trị
            int size = BitConverter.ToInt32(datasize, 0); 
            int dataleft = size;
            byte[] data = new byte[size]; // tạo mảng byte để nhận dữ liệu từ server

            while (total < size)
            {

                // Nhận số byte được chỉ định từ Socket được ràng buộc vào vị trí bù đã chỉ định của bộ đệm nhận, sử dụng SocketFlags được chỉ định.
                receive = socket.Receive(data, total, dataleft, 0);
                if (receive == 0) // nếu số byte trả về bằng 0 thì biến đổi chuỗi exit thành mảng byte và dừng vòng lặp
                {
                    data = Encoding.ASCII.GetBytes("exit ");
                    break;
                }
                total += receive;
                dataleft -= receive;
            }
            return data;
        }

        public static void Main()
        {
            Console.Title = "VarTcpClient";
            byte[] data = new byte[1024];
            int sent;

            // Thiết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);

            // Khởi tạo object của lớp Socket để sử dụng dịch vụ Tcp
            // Lưu ý Socket Type của Tcp là Stream
            // InterNetWork là họ địa chỉ dành cho IPv4
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep); // Tạo kết nối với server
            }
            catch (SocketException e)
            {
                Console.WriteLine("Unable to connect to server.");
                Console.WriteLine(e.ToString());
                return;
            }

            data = ReceiveVarData(server);

            // Chuyển đổi mảng byte data về chuỗi
            string stringData = Encoding.ASCII.GetString(data);
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Write("$ Receive >> ");
            Console.ResetColor();
            Console.Write(stringData + "\r\n");
            string message1 = "This is the first test";
            string message2 = "A short test";
            string message3 = "This string is an even longer test. The quick brown fox jumps over the lazy dog.";
            string message4 = "hello";
            string message5 = "The last test";
            string message6 = "Test";
            string message7 = "Thest";

            sent = SendVarData(server, Encoding.ASCII.GetBytes(message1));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message2));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message3));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message4));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message5));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message6));
            sent = SendVarData(server, Encoding.ASCII.GetBytes(message7));

            Console.WriteLine("Disconnecting from server...");
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }
    }
}
