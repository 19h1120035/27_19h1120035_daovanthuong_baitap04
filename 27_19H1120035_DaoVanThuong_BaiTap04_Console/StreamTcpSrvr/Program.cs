﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace StreamTcpSrvr
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "StreamTcpServer";
            string data;

            // 1. Thết lập IPEndPoint và Socket
            // Khởi tạo IPEndPoint với địa chỉ IP và port được chỉ định
            // Giá trị Any của IPAddress tương ứng với  IP của tất cả giao diện mạng trên máy
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);

            // Tcp sử dụng đồng thời hai Socket
            // 1 Socket để chờ nghe kết nối, một Socket để gửi/nhận dữ liệu
            // Socket server này chỉ làm nhiệm vụ chờ kết nối từ Client
            // Socket Type của Tcp là Stream
            Socket newsock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // 2. Kết nối Socket với IPEndPoint
            // Yều cầu hệ điều hành cho phép chiếm dụng cổng Tcp 6969
            // Server sẽ nghe trên tất cả các mạng mà máy tính này kết nối tới 
            // chỉ cần gói tin Tcp đến cổng 9050, tiến trình server sẽ nhận được
            newsock.Bind(ipep);

            // 3. Lắng nghe từ client
            newsock.Listen(10); // Lắng nghe tối đa từ 10 client
            Console.WriteLine("Waiting for a client...");

            // 4. Chấp nhận kết nối từ client
            // Tcp đòi hỏi 1 Socket thứ hai làm nhiệm vụ gửi/nhận dữ liệu   
            Socket client = newsock.Accept();


            // Lấy IPEndpoint và trả về EndPoint mà Socket đang giao tiếp
            IPEndPoint newclient = (IPEndPoint)client.RemoteEndPoint;

            // Lấy địa chỉ IP và port của Endpoint 
            Console.WriteLine("Connected with {0} at port {1}", newclient.Address, newclient.Port);

            // Khởi tạo NetworkStream với Socket được chỉ định dùng để gửi và nhận dữ liệu
            NetworkStream ns = new NetworkStream(client);

            // Khởi tạo StreamReader cho luồng được chỉ định để đọc
            StreamReader sr = new StreamReader(ns);

            // Khởi tạo StreamWriter cho luồng được chỉ định để viết
            StreamWriter sw = new StreamWriter(ns);

            string welcome = "Welcome to my test server";
            sw.WriteLine(welcome); // ghi chuỗi welcome vào luồng

            // Xóa tất cả các bộ đệm cho trình ghi hiện tại
            // và khiến mọi dữ liệu trong bộ đệm được ghi vào luồng bên dưới.
            sw.Flush();

            while (true)
            {
                try
                {
                    // Đọc một dòng ký tự từ luồng hiện tại và trả về dữ liệu dưới dạng một chuỗi
                    data = sr.ReadLine();
                    if (data.ToUpper().Equals("EXIT")) break;
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.Write("$ Client >>> ");
                    Console.ResetColor();
                    Console.Write(data + "\r\n");

                    // gửi lại chuỗi in hoa cho client
                    sw.WriteLine(data.ToUpper()); // Ghi chuỗi data vào luồng sw 

                    // Xóa tất cả các bộ đệm cho trình ghi hiện tại
                    // và khiến mọi dữ liệu trong bộ đệm được ghi vào luồng bên dưới.
                    sw.Flush();
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            Console.WriteLine("Disconnected from {0}", newclient.Address);
            sw.Close(); // Đóng StreamWriter hiện tại và luồng bên dưới
            sr.Close();
            ns.Close();
        }
    }
}
